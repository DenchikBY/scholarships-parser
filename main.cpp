#include "string"
#include "iostream"
using namespace std;

#include "curl/curl.h"
#pragma comment(lib, "curllib.lib")

#include "rapidjson/document.h"

#include "sstream"
#include "fstream"

#include "pugihtml/pugiconfig.hpp"
#include "pugihtml/pugihtml.hpp"

#include "tidy/tidy.h"
#include "tidy/buffio.h"
#include "stdio.h"
#include "errno.h"
#pragma comment(lib, "libtidy.lib")

#include <process.h>
#include <chrono>
#include <thread>

bool file_exist(const std::string& name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return true;
	}
	else
	{
		return false;
	}
}

size_t write_to_string(void *ptr, size_t size, size_t count, void *stream) {
	((string*)stream)->append((char*)ptr, 0, size*count);
	return size*count;
}

string file_get_contents(const char *filename)
{
	ifstream in(filename);
	if (in.fail())
	{
		cerr << "File not found: " << filename << endl;
		return "";
	}
	std::stringstream buffer;
	buffer << in.rdbuf();
	in.close();
	return buffer.str();
}

void file_put_contents(char * fileName, char * content)
{
	ofstream file;
	file.open(fileName);
	file << content;
	file.close();
}

char* string_to_char(string string)
{
	char *cstr = new char[string.length() + 1];
	strcpy(cstr, string.c_str());
	return cstr;
}

char* tidy_html(char* input)
{
	TidyBuffer output = { 0 };
	TidyBuffer errbuf = { 0 };
	int rc = -1;
	Bool ok;
	TidyDoc tdoc = tidyCreate();
	ok = tidyOptSetBool(tdoc, TidyXhtmlOut, yes);
	if (ok)
		rc = tidySetErrorBuffer(tdoc, &errbuf);
	if (rc >= 0)
		rc = tidyParseString(tdoc, input);
	if (rc >= 0)
		rc = tidyCleanAndRepair(tdoc);
	if (rc >= 0)
		rc = tidyRunDiagnostics(tdoc);
	if (rc > 1)
		rc = (tidyOptSetBool(tdoc, TidyForceOutput, yes) ? rc : -1);
	if (rc >= 0)
		rc = tidySaveBuffer(tdoc, &output);
	return (char*)output.bp;
}

bool replace(std::string& str, const std::string& from, const std::string& to)
{
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
	{
		return false;
	}
	str.replace(start_pos, from.length(), to);
	return true;
}

string num_ranks(string s)
{
	string str = (string)s;
	int len = str.length();
	for (int i = 0; i < len / 3; ++i)
	{
		str.insert(len - 3 * (i + 1), ".");
	}
	return str;
}

void utf8win1251(const char *in, char *out)
{
	unsigned char i;
	unsigned long c1 = 32, new_c1, new_c2, new_i, out_i;
	bool byte2 = false;
	int c_out = 0;
	for (int c = 0; c<strlen(in); c++)
	{
		i = in[c];
		if (i <= 127) out[c_out++] = in[c];
		if (byte2)
		{
			new_c2 = (c1 & 3) * 64 + (i & 63);
			new_c1 = (c1 >> 2) & 5;
			new_i = new_c1 * 256 + new_c2;
			if (new_i == 1025)
			{
				out_i = 168;
			}
			else
			{
				if (new_i == 1105)
				{
					out_i = 184;
				}
				else
				{
					out_i = new_i - 848;
				}
			}
			out[c_out++] = (char)out_i;
			byte2 = false;
		}
		if ((i >> 5) == 6)
		{
			c1 = i;
			byte2 = true;
		}
	}
	out[c_out] = 0;
}

void clear_screen()
{
	#ifdef _WIN32
		std::system("cls");
	#else
		std::system("clear");
	#endif
}

unsigned __stdcall second_thread(void*)
{
	int i = 0;
	while (1)
	{
		clear_screen();
		string s = "��������";
		for (int i1 = 0; i1 < i % 3 + 1; ++i1)
		{
			s += ".";
		}
		++i;
		cout << s << endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	_endthreadex(0);
	return 0;
}

HANDLE hThread;

void stop_second_thread()
{
	TerminateThread(hThread, NULL);
	CloseHandle(hThread);
}

void write_msg(string message)
{
	stop_second_thread();
	clear_screen();
	cout << message << endl;
}

void main()
{
	setlocale(0, ".1251");
	if (!file_exist("settings.txt"))
	{
		cout << "����������� ���� ��������" << endl;
	}
	else
	{
		char* json = string_to_char(file_get_contents("settings.txt"));
		rapidjson::Document settings;
		if (settings.Parse<0>(json).HasParseError())
		{
			cout << "Json parse error." << endl;
		}
		else
		{
			if (!settings["login"].IsString() || !settings["password"].IsString() || !settings["numbers"].IsArray() || settings["numbers"].Size() < 40)
			{
				cout << "Settings is not full" << endl;
			}
			else
			{
				CURL *curl = curl_easy_init();
				if (!curl)
				{
					cout << "Curl init error." << endl;
				}
				else
				{
					unsigned threadID;
					hThread = (HANDLE)_beginthreadex(NULL, 0, &second_thread, NULL, 0, &threadID);
					string response;
					char* response_char;
					pugihtml::html_document doc;
					pugihtml::html_parse_result result;
					char* cookie_path = string_to_char("cookie.txt");
					curl_easy_setopt(curl, CURLOPT_URL, "https://ibank.asb.by/wps/portal/ibank/");
					curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
					curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
					curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookie_path);
					curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookie_path);
					curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
					curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
					curl_easy_perform(curl);
					response_char = tidy_html(string_to_char(response));
					result = doc.load(response_char);
					if (!result)
					{
						write_msg("DOM(1) load error: " + (string)result.description());
					}
					else
					{
						pugihtml::xpath_node form = doc.select_single_node("//FORM[@NAME='LoginForm1']");
						if (!form)
						{
							write_msg("XPath form(1) find error.");
						}
						else
						{
							string form_action = (string)form.node().attribute("ACTION").value();
							curl_easy_setopt(curl, CURLOPT_URL, string_to_char("https://ibank.asb.by" + form_action));
							curl_easy_setopt(curl, CURLOPT_POST, 1);
							curl_easy_setopt(curl, CURLOPT_POSTFIELDS, string_to_char(
								"bbIbUseridField=" + (string)settings["login"].GetString() +
								"&bbIbPasswordField=" + (string)settings["password"].GetString() +
								"&bbIbLoginAction=in-action" +
								"&bbIbCancelAction=" +
								"&bbibUnblockAction="
							));
							response.clear();
							curl_easy_perform(curl);
							response_char = tidy_html(string_to_char(response));
							result = doc.load(response_char);
							if (!result)
							{
								write_msg("DOM(2) load error: " + (string)result.description());
							}
							else
							{
								pugihtml::xpath_node form = doc.select_single_node("//FORM[@NAME='LoginForm1']");
								if (!form)
								{
									write_msg("XPath form(2) find error.");
								}
								else
								{
									string num_text = (string)doc.select_single_node("//LABEL[@FOR='codevalue']").node().child_value();
									if (num_text.empty())
									{
										write_msg("Login or password are incorrect");
									}
									else
									{
										string form_action = (string)form.node().attribute("ACTION").value();
										char* num = string_to_char(num_text.substr(num_text.find_last_of(" ") + 1));
										curl_easy_setopt(curl, CURLOPT_URL, string_to_char("https://ibank.asb.by" + form_action));
										curl_easy_setopt(curl, CURLOPT_POSTFIELDS, string_to_char((string)
											"cancelindicator=false" +
											"&bbIbCodevalueField=" + settings["numbers"][atoi(num) - 1].GetString() +
											"&bbIbLoginAction=in-action" +
											"&bbIbCancelAction=" +
											"&field_1=" + doc.select_single_node("//INPUT[@ID='field_1']").node().attribute("VALUE").value() +
											"&field_2=" + doc.select_single_node("//INPUT[@ID='field_2']").node().attribute("VALUE").value() +
											"&field_3=" + doc.select_single_node("//INPUT[@ID='field_3']").node().attribute("VALUE").value() +
											"&field_4=" + doc.select_single_node("//INPUT[@ID='field_4']").node().attribute("VALUE").value() +
											"&field_5=" + doc.select_single_node("//INPUT[@ID='field_5']").node().attribute("VALUE").value() +
											"&code_number_expire_time=false"
										));
										response.clear();
										curl_easy_perform(curl);
										response_char = tidy_html(string_to_char(response));
										result = doc.load(response_char);
										if (!result)
										{
											write_msg("DOM(3) load error: " + (string)result.description());
										}
										else
										{
											string name_block = (string)doc.select_single_node("//DIV[@CLASS='block_in']/P[1]").node().child_value();
											if (name_block.empty())
											{
												write_msg("Number are incorrect");
											}
											else
											{
												string name = name_block.substr(name_block.find_first_of(", ") + 2);
												name = name.substr(0, name.length() - 1);
												replace(name, "\n", " ");
												char name1[500];
												utf8win1251(string_to_char(name), name1);
												stop_second_thread();
												clear_screen();
												cout << (string)name1 << endl;
												pugihtml::xpath_node_set cards = doc.select_nodes("//DIV[@CLASS='block_bottom_in']/TABLE[1]/TBODY[1]/TR");
												printf("-------------------------------------------------------------\n");
												printf("| %3s | %20s | %15s | %10s |\n", "#", "����� �����", "�������", "������");
												printf("-------------------------------------------------------------\n");
												for (int i = 0; i < cards.size(); ++i)
												{
													pugihtml::xpath_node_set td = cards[i].node().select_nodes("TD/DIV");
													printf("| %3s | %20s | %15s | %10s |\n", td[0].node().child_value(), td[1].node().child_value(), string_to_char(num_ranks((string)td[2].node().child_value())), td[3].node().child_value());
													printf("-------------------------------------------------------------\n");
												}
											}
										}
									}
								}
							}
						}
					}
					curl_easy_cleanup(curl);
				}
			}
		}
	}
	system("pause");
}